import axios from "axios";

const api = axios.create({
  baseURL: "http://10.89.234.175:5000/api/reservas/v1/",
  headers: {
    accept: "application/json",
  },
});

const userAPI = {
  //User
  createUser: (userData) => api.post("user/", userData),
  loginUser: (userData) => api.post("user/login", userData),
  getAllUsers: () => api.get("user/"),
  getUserById: (userId) => api.get(`user/${userId}`),
  updateUser: (userId, userData) => api.put(`user/${userId}`, userData),
  deleteUser: (userId) => api.delete(`user/${userId}`),

  //Classroom
  getAllClassroom: () => api.get("classroom/"),
  getAllClassroomById: (classroomId) => api.get(`classroom/${classroomId}`),

  //Schedule
  createSchedule: (schedule) => api.post("schedule/", schedule),
  getScheduleByClassroomId: (classroomId) => api.get(`schedule/${classroomId}`),
  getSchedulesByIdClassroomRanges: (classroomId, weekStart,weekEnd) =>
    api.get(`schedule/ranges/${classroomId}`, { params: { weekStart,weekEnd } }),
 deleteSchedule:(scheduleId) => api.delete(`schedule/${scheduleId}`),
};

export default userAPI;
