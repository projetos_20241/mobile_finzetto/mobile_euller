import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet, Text , Alert} from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [credentials, setCredentials] = useState({ email: "", password: "" });

  const handleLogin = () => {

    // Exemplo de chamada para criar um usuário com os dados do formulário de login
    api.loginUser(credentials)
      .then(response => {
        console.log(response.data); // Exibe a resposta da API no console
        Alert.alert("Bem vindo",response.data.message)
        navigation.navigate("HomePage", { user: response.data.user.cpf }); // Vá para a próxima tela após Login
      })
      .catch(error => {
        Alert.alert("Erro",error.response.data.error);
      });
  };

  const handleSignupRedirect = () => {
    navigation.navigate("SignupScreen");
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setCredentials({ ...credentials, email: text })
        }
        value={credentials.email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setCredentials({ ...credentials, password: text })
        }
        value={credentials.password}
        secureTextEntry
      />

      <View style={styles.buttonContainer}>
        <Button title="Login" onPress={handleLogin} color="" />
        <Button title="Cadastre-se" onPress={handleSignupRedirect} color="green" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#084d6e",
  },
  input: {
    width: "80%",
    marginBottom: 10,
    padding: 15,
    color: "#fff",
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 10,
  },
  buttonContainer: {
    marginVertical: 10, // Adiciona espaçamento vertical entre os botões
  },
  
  
});

export default HomeScreen;