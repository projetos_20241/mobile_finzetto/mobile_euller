import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet , Alert} from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const SignupScreen = () => {
  const navigation = useNavigation();
  const [user, setUser] = useState({ cpf: "", email: "", password: "", name: "" });

  const handleSignup = () => {
    api.createUser(user)
      .then(response => {
        console.log(response.data); // Exibe a resposta da API no console
        Alert.alert("Faça Login",response.data.message);
        navigation.navigate("Home"); // Navega para a próxima tela após criar o usuário
      })
      .catch(error => {
        console.log(error)
        Alert.alert("Erro",error.response.data.error);
      });
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="CPF"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setUser({ ...user, cpf: text })
        }
        value={user.cpf}
      />
      <TextInput
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setUser({ ...user, email: text })
        }
        value={user.email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setUser({ ...user, password: text })
        }
        value={user.password}
        secureTextEntry
      />
      <TextInput
        style={styles.input}
        placeholder="Name"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setUser({ ...user, name: text })
        }
        value={user.name}
      />
      <Button title="Cadastre-se" onPress={handleSignup} color="green" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#084d6e",
  },
  input: {
    width: "80%",
    marginBottom: 10,
    padding: 15,
    color: "#fff",
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 10,
  },
});

export default SignupScreen;